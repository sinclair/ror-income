class CreateActs < ActiveRecord::Migration
  def change
    create_table :acts do |t|
      t.string  :label
      t.integer :user_id
      t.float   :amount
      t.integer :category_id
      t.boolean :income
      t.timestamps
    end
    add_index :acts, :user_id
    add_index :acts, :amount
    add_index :acts, :income
  end
end
