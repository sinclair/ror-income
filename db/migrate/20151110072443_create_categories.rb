class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string  :title
      t.integer :user_id
      t.boolean :income
      t.timestamps
    end
    add_index :categories, :user_id
    add_index :categories, :income
  end
end
