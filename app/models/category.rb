class Category < ActiveRecord::Base
  has_many :act
  belongs_to :user

  validates :user_id,     numericality: {greater_than: 0, allow_nil: true, presence: true}
  validates :title,       :presence => true,
            :length => {:minimum => 1, :maximum => 254}
end
