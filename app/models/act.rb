class Act < ActiveRecord::Base
  belongs_to :user
  belongs_to :category

  validates :amount,      numericality: {greater_than: 0, allow_nil: true, presence: true}
  validates :user_id,     numericality: {greater_than: 0, allow_nil: true, presence: true}
  validates :category_id, numericality: {greater_than: 0, allow_nil: true}
  validates :label,       :length => {:minimum => 0, :maximum => 254}
end