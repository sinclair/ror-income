class CategoriesController < ApplicationController
  before_filter :find_category, only:[:show, :edit, :update, :destroy]
  before_filter :check_admin, only:[:show, :edit, :update, :destroy]
  def index
    @categories = Category.where(user_id:current_user.id)
  end


  # /categories POST
  def create
    @category = Category.new
    @category.title = params[:category][:title]
    @category.user_id = current_user.id
    @category.income = params[:category][:income]
    @category.save
    if @category.errors.empty?
      redirect_to categories_path
    else
      render "new"
    end
  end

  # /categories/1/edit GET
  def edit
    @income = false
  end

  # /categories/1 PUT
  def update
    @category.update_attributes(category_params)
    if @category.errors.empty?
      redirect_to categories_path
    else
      render "edit"
    end
  end

  # /categories/1 DELETE
  def destroy
    @category.destroy
    redirect_to categories_path
  end

  private

  def find_category
    @category = Category.find(params[:id])
  end

  def check_admin
    render text: "Access Denied", status: 403 unless @category.user_id == current_user.id
  end

  def category_params
    params.require(:category).permit(:title,:user_id,:income)
  end
end
