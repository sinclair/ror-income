class ActsController < ApplicationController

  before_filter :find_current_action, only:[:show, :edit, :update, :destroy]
  before_filter :check_admin, only:[:show, :edit, :update, :destroy]
  def index
    @actions = Act
                   .select('acts.*, categories.title as category_title')
                   .joins(:category)
                   .where(user_id:current_user.id)
  end

  # /acts POST
  def create
    @current_action = Act.new
    @current_action.label = params[:act][:label]
    @current_action.amount = params[:act][:amount]
    @current_action.user_id = params[:act][:user_id]
    @current_action.category_id = params[:act][:category_id]
    @current_action.income = params[:act][:income]
    @current_action.save
    if @current_action.errors.empty?
      redirect_to root_path
    else
      render "new"
    end
  end

  # /acts/1/edit GET
  def edit
    @categories = Category.where(user_id:current_user.id,income:@current_action.income)
  end

  # /acts/1 PUT
  def update
    @current_action.update_attributes(act_params)
    if @current_action.errors.empty?
      redirect_to root_path
    else
      render "edit"
    end
  end

  # /acts/1 DELETE
  def destroy
    @current_action.destroy
    redirect_to root_path
  end

  private

  def find_current_action
    @current_action = Act.find(params[:id])
  end

  def check_admin
    render text: "Access Denied", status: 403 unless @current_action.user_id == current_user.id
  end

  def act_params
    params.require(:act).permit(:amount,:label,:category_id,:user_id,:income)
  end

end
