class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_balance
  before_filter :get_categories
  before_filter :get_actions

  def after_sign_in_path_for(resource)
    root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    request.referrer
  end

  private

  def set_balance
    if user_signed_in?
      @actions = Act.where(user_id: current_user.id)
      $balance = 0
      @actions.each do |b|
        if b.income?
          $balance = $balance +b.amount
        else
          $balance = $balance -b.amount
        end
      end
    else
    end
  end

  def get_categories
    if user_signed_in?
    @income_categories = Category.where(user_id:current_user.id,income:true)
    @expence_categories = Category.where(user_id:current_user.id,income:false)
    end
  end

  def get_actions
    if user_signed_in?

      if params['search_end_period']
        arr_end_period = params['search_end_period'].join.split("-")
        $search_end_period = Date.new(arr_end_period[0].to_i,arr_end_period[1].to_i,arr_end_period[2].to_i).to_time.to_i
      else
        $search_end_period = Time.now.to_time.to_i
      end

      if params['search_start_period']
        arr_start_period = params['search_start_period'].join.split("-")
        $search_start_period = Date.new(arr_start_period[0].to_i, arr_start_period[1].to_i, arr_start_period[2].to_i).to_time.to_i
      else
        $search_start_period = 0
      end


#render text: params['search_start_period']
        @actions = Act
                       .select('acts.*, categories.title as category_title')
                       .joins("left join categories on categories.id = acts.category_id")
                       .where(user_id:current_user.id)
                       .where("acts.created_at >= to_timestamp(:created_at)", {created_at: $search_start_period })
                       .where("acts.created_at <= to_timestamp(:created_at)", {created_at: $search_end_period })
                       .order("created_at DESC")
    end
  end

end
