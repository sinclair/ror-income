class HomeController < ApplicationController
  def index
    if user_signed_in?
      @all_categories = Category.where(user_id:current_user.id)
    end
  end
end
